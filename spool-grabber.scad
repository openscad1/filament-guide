// NOSTL

include <libopenscad/mcube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/mtube.scad>;

inch = 2.54 * 10;


spool_outer_diameter = 200;
spool_width = 68;
spool_core_diameter = inch * 2;
spool_core_wall_thickness = 8;

default_spool_retainer_reach = 3;
default_spool_retainer_cap_height = 4;



default_arm_height = 4;
default_arm_width = 40;
default_arm_length = 120;
default_arm_position = .6;

//default_more_gap = 2;
default_more_gap = 0;

default_chamfer = default_arm_height / 4;

slot_usable_length = 25;

debug = true;
//debug_beam_slug = true;
//debug_spool_slug = true;
debug_beam = true;
//debug_arm = true;
//debug_no_hull = true;
//debug_wedge = true;
//debug_ring = true;
//debug_spacer = true;



$fn = is_undef(debug) || debug == false ? 90 : 30;


module spool_post(arm_height = default_arm_height, arm_width = default_arm_width, spool_retainer_cap_height = default_spool_retainer_cap_height, spool_retainer_reach = default_spool_retainer_reach, more_gap = default_more_gap, chamfer = default_chamfer) {
    
    // translate([0, 0, -arm_height]) color("cyan") mcylinder(d = spool_core_diameter, h = spool_core_wall_thickness + spool_retainer_cap_height + arm_height + 20, align = [0, 0, 1], chamfer = chamfer);
       

    intersection() {
        union() {
            for(x = [-1, 1]) {
            
                translate([x * more_gap / 2, 0, 0]) {
                    color("lightblue") {
                        // central cylinder
                        translate([0, 0, -arm_height]) {
                            mcylinder(d = spool_core_diameter, h = spool_core_wall_thickness + spool_retainer_cap_height + arm_height, align = [0, 0, 1], chamfer = chamfer);
                        }
                    }

                    color("salmon") {
                        // flared retainer

                        hull() {
                            translate([0, 0, spool_core_wall_thickness ]) {
                                // "inner" (smaller/lower) ring
                                mcylinder(d = spool_core_diameter, h = spool_retainer_cap_height, align = [0, 0, 1], chamfer = chamfer);

                                translate([0, 0, spool_retainer_reach]) {
                                    // "outer" (larger/upper) ring
                                    mcylinder(d = spool_core_diameter + (default_spool_retainer_reach * 2), h = spool_retainer_cap_height, align = [0, 0, 1], chamfer = chamfer);
                                }
                            }
                        }
                    }
                }
            } // for()
                color("orange") {
            mcylinder(d = spool_core_diameter + spool_retainer_reach * 2 + chamfer * 2, h = arm_height, align = [0, 0, -1], chamfer = chamfer);
    }

        } // union()


        union() {
            // a little off the top, more off the bottom
            mcube([spool_core_diameter * 2, spool_core_diameter * .75, spool_core_wall_thickness * 10], align = [0, 0.2, 0], color = "red");
        }
    }

 
}




module stick(arm_height = default_arm_height, arm_width = default_arm_width, arm_length = default_arm_length, arm_position = default_arm_position, flange_depth = default_arm_height, shrinkage = 0.975, chamfer = default_chamfer) {
$fn=180;
    
    color("lightgreen") {
        // main "tongue depresser"
        mcube([arm_width, arm_length + chamfer, arm_height], align = [0, 1, -1], chamfer = chamfer);
    }

    /*
    // arched slot beam flange v1
    translate([0, default_arm_length - slot_usable_length / 2 + flange_depth * 1.25, 0]) {
        for(x = [-1, 1]) {
            translate([x * default_arm_width / 6, 0, 0]) {
                rotate([90, 0, (x * 90) - 90]) {
                    difference() {
                        color("LightSlateGray") {

                            intersection() {
                                scale([1,.875,1]) mcylinder(h = slot_usable_length - flange_depth * 3, d = default_arm_height * 6, align = [0, 0, 0], chamfer = default_arm_height / 2);
                                mcube([default_arm_height * 3, default_arm_height * 3, slot_usable_length + 1], align = [1, 1, 0]);
                            }
                        }

                        color("red") { 
                            mcube([default_arm_height * 2, default_arm_height * 2 + 0.5, slot_usable_length * 2], align = [0, 0, 0], chamfer = default_arm_height * 0.375);
                        }
                    }
                }
            }
        }
    }
    */




    // retainer ring
translate([0, default_arm_length - default_spool_retainer_reach * 2])
rotate([0, 0, 90])
difference() {
    color("purple"){

        union() {
            translate([0, 0, -default_arm_height]) {
                mtube(od = default_arm_width, id = default_spool_retainer_reach * 5, h = default_arm_height * 3.5, angle = 180, align = [0, 0, 1], chamfer = default_arm_height / 2);
                /*
                for(y = [-1, 1]) {
                    translate([0, y * default_arm_width / 2, 0]) {
                        mcylinder(h = default_arm_height * 3, d = (default_arm_width - default_spool_retainer_reach * 4)/2, align = [0, -y, 1], chamfer = default_arm_height / 2);
                    }
                }
                */
            }
        }
    }

    color("pink") {
        union() {
            translate([0, 0, - default_arm_height * 2]) {
                    mcylinder(d = (default_spool_retainer_reach * 2 + flange_depth) * 2 + (2 - shrinkage), h = default_arm_height * 4 + (1 - shrinkage), align = [0, 0, 1], chamfer = default_arm_height* 1.35 );
            }
        }
    }
}

    

    color("orange") {
        // rounded top
        translate([0, arm_length , 0]) {
            mcylinder(d = arm_width, h = arm_height, align = [0, 0, -1], chamfer = chamfer);
        }
    }

}



module ring() {
    
    c=default_spool_retainer_reach / 4;
    hh = default_arm_height * 3.5/2;
    echo(hh);
    floor_thickness = 4.5;
    offset = 10;

    translate([0, default_arm_length  - default_spool_retainer_reach * 2, 0]) {
        
        a = true; 
        b = true;
        
        if(!is_undef(a) && a) {
            intersection() {
                color("Tomato") {
                    // rounded top
                    mtube(h = hh, od = default_arm_width + default_spool_retainer_reach * 2, id = default_arm_width, align = [0, 0, 1], chamfer = c);
                    }
                color("cyan") {
                    translate([0, -offset, 0]) {
                        mcube([100, 100, hh], align = [0, 1, 1], chamfer = c);
                    }
                }
            }
        }

        if(!is_undef(b) && b) {
            intersection() {        
                color("Lime") {
                    intersection() {
                        mcylinder(h = hh, d = default_arm_width + default_spool_retainer_reach, align = [0, 0, 1], chamfer = c);
                        translate([0, floor_thickness, 0]) {
                            mcube([100, 100, 100], align = [0, -1, 0]);
                        }
                    }
                }
        
                color("red") {
                    translate([0, -offset, 0]) {
                        mcube([100, floor_thickness, hh], align = [0, 1, 1], chamfer = c);
                    }
                }
            }
        }

    }
    
}




module arm() {
    difference() {

        union() {
            spool_post();
            stick();
        }

        union() {
            // cutout
            translate([0, -(default_arm_height + default_chamfer), -(default_arm_height + 1)]) {
                foo = default_arm_height + spool_core_wall_thickness + default_spool_retainer_reach + default_spool_retainer_cap_height;
                
                
                mcube([default_spool_retainer_reach * 4, default_arm_length * 2 + 1, foo + 2], align = [0, 0, 1], color = "red");
                translate([0, default_arm_length , 0]) {
                    mcylinder(d = default_spool_retainer_reach * 4, h = foo + 2, align = [0, 0, 1]);
                }
            }
        }
        
    }
}







module beam(radius = default_spool_retainer_reach * 2 , width = slot_usable_length - default_arm_height * 2, length = spool_width + default_arm_height * 2, flange_offset = default_arm_height, flange_depth = default_arm_height , flange_height = default_arm_height, shrinkage = 1.025 /* 0.975 */, bonus_width = 0) {
$t  = .5;
    translate([0, default_arm_length - width / 2 + flange_depth/4 -(17 * abs((0.5-$t))), length / 2 - default_arm_height]) {

        total_length = length + bonus_width;
        echo(length, total_length);
        difference() {
            union() {
                // +beam
                color("fuchsia") {
                    // shaft
                    width = slot_usable_length - default_arm_height * 2;
                    difference () {
                        union() {
                            if(!is_undef(debug_no_hull) && debug_no_hull) {
                                echo("debug: nohull");
                                union() {
                                    translate([0,   width/2, 0]) mcylinder(d = radius * 2 * shrinkage, h = total_length, align = [0, -1, 0], chamfer = flange_height / 4);
                                    translate([0, - width/4, 0]) mcube([radius * 2 * shrinkage, radius * shrinkage, total_length], align = [0, 1, 0], chamfer = flange_height / 4);
                                }
                            } else {
                                hull() {
                                    translate([0,   width/2, 0]) mcylinder(d = radius * 2 * shrinkage, h = total_length, align = [0, -1, 0], chamfer = flange_height / 4);
                                    translate([0, - width/4, 0]) mcube([radius * 2 * shrinkage, radius * shrinkage, total_length], align = [0, 1, 0], chamfer = flange_height / 4);
                                }
                            }
                        }

                        union() {
                            if(bonus_width) {
                                translate([0, -radius/2, 0]) {
                                    rotate([90, 90, 0]) {
                                        linear_extrude(height = 10) {
                                            text(str("+", bonus_width), halign = "center", valign = "center", size = 7, font = "Liberation Mono:style=Bold");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                color("blue") {
                    // bell ends
                    for(z = [-1, 1]) {
                        translate([0, 0, z * ((total_length / 2) - flange_offset)]) {
                            if(!is_undef(debug_no_hull) && debug_no_hull) {
                                echo("debug: nohull");
                                    union() {
                                    translate([0, (width/2 + flange_depth), 0]) {
                                        intersection() {
                                            mcylinder(d = (radius + flange_depth) * 2, h = flange_height, align = [0, -1, -z], chamfer = flange_height / 4);
                                            mcube([(radius + flange_depth) * 2, (radius + flange_depth), flange_height], align = [0, -1, -z], chamfer = flange_height / 4);
                                        }
                                    }
                                    translate([0, -1 * (width/4), 0]) mcube([(radius + flange_depth) * 2, (radius + flange_depth*0), flange_height], align = [0, 1, -z], chamfer = flange_height / 4);
                                }
                            } else {
                                hull() {
                                    translate([0, (width/2 + flange_depth), 0]) {
                                        intersection() {
                                            mcylinder(d = (radius + flange_depth) * 2, h = flange_height, align = [0, -1, -z], chamfer = flange_height / 4);
                                            mcube([(radius + flange_depth) * 2, (radius + flange_depth), flange_height], align = [0, -1, -z], chamfer = flange_height / 4);
                                        }
                                    }
                                    translate([0, -1 * (width/4), 0]) mcube([(radius + flange_depth) * 2, (radius + flange_depth*0), flange_height], align = [0, 1, -z], chamfer = flange_height / 4);
                                }
                            }
                        }
                    }
                }
            }


            union() {
                // -chowthink
                translate([0, 2, 0])
                color("red") {
                    // urethrae
                    rotate([0, 90, 30]) {
                        mcylinder(d = 4, h = 100);
                    }
                    rotate([0, 90, 0]) {
                     //   mcylinder(d = 5, h = 100);
                    }
                }
            }
        }
    }
}






module spacer(len = 25) {
    
    color("orange") {
        h = len;
        id = 12;
        od = 12 + (25.4 / 8) * 2;
        c = ((od - id) / 2) / 4;
        
        mtube(h = h, od = od, id = id, chamfer = c);
       
    }    
}


if(!is_undef(debug) && debug) {
    
    if(!is_undef(debug_spacer) && debug_spacer) {
        spacer();
    }

    
    
    
    if(!is_undef(debug_arm) && debug_arm) {
        arm();
    }

    if(!is_undef(debug_beam) && debug_beam) {
        beam();
    }

    if(!is_undef(debug_wedge) && debug_wedge) {
//        wedge();
    }

    if(!is_undef(debug_ring) && debug_ring) {
        ring();
    }



    if(!is_undef(debug_beam_slug) && debug_beam_slug) {
        color("cyan") {
            hull() {
                translate([0, default_arm_length + default_chamfer, 0]) {
                    mcylinder(d = default_spool_retainer_reach * 4, h= 10, align = [0, -1, 0]);
                    translate([0, -slot_usable_length, 0]) {
                        mcylinder(d = default_spool_retainer_reach * 4, h= 10, align = [0, 1, 0]);
                    }
                }
            }
        }
    }


    if(!is_undef(debug_spool_slug) && debug_spool_slug) {
        color("gray") {
            translate([0, 0, spool_width/2]) {
                difference() {
                    mcylinder(d = spool_outer_diameter, h = spool_width, align = [0, 0, 0]);
                    mcylinder(d = spool_core_diameter, h = spool_width + 1, align = [0, 0, 0]);
                }
            }
        }
    }
}

