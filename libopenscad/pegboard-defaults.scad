/*

Default values observed to work with [Alligator
Board](https://www.alligatorboard.com/) and [Wall
Control](https://www.wallcontrol.com/) metal pegboard, based on a one
inch grid pattern.

"base_size" refers to the basic amount of space from hole to hole (one
inch).  Many pegboard components are expressed in multiples of
"base_size".

"plate_thickness" is a wall thickness (1/8 inch) that has been
observed to work well.  This thickness has been observed to be strong,
slightly flexible, and make reasonable use of material volume.

The author has found objects to be scaled on inch multiples with 1/8
inch thick wall to be pleasing and practical.  All work has been
rendered on a Prusa Mini.

Michael Allen - 2021

*/



base_size = 25.4;
plate_thickness = base_size / 8;
manifold_overlap = 0.01;



// EOF
