use <libopenscad/mcylinder.scad>;

d = 50;

squash = 0.250;

$fn = 90;
difference() {
color("royalblue") {
    scale([1,1,squash]) {
        sphere(d=d);
    }
}
color("red") {
    mcylinder(d = d + 2, h = (d + 2) / 2, align = [0, 0, -1]);
}
}